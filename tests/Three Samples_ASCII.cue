REM GENRE "Noise"
REM DATE 2021
REM COMMENT "Just a little bit of noise generated with sox"
PERFORMER "Sox the genius"
TITLE "Sox - Three samples"
FILE "č Ř ž š ť ň Ů Three Samples.flac" WAVE
  TRACK 01 AUDIO
    TITLE "č Ř ž š ť ň Ů 300 Hz"
    INDEX 01 00:00:00
  TRACK 02 AUDIO
    TITLE "č Ř ž š ť ň Ů 400 Hz"
    INDEX 01 00:01:00
  TRACK 03 AUDIO
    TITLE "č Ř ž š ť ň Ů 500 Hz"
    INDEX 01 00:02:00
